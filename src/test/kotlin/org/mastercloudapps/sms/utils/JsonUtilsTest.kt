package org.mastercloudapps.sms.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.entity.Post
import org.mastercloudapps.sms.extensions.toDto
import org.mastercloudapps.sms.util.FileUtils
import org.mastercloudapps.sms.util.JsonUtils

class JsonUtilsTest {

    @Test
    fun `json serialize`() {
        val posts: List<Post> = getPosts()
        assertThat(posts.size).isEqualTo(2)
        assertThat(posts[1].text).isEqualTo(MESSAGE_2)
    }

    @Test
    fun `json deserialize`() {
        val posts: List<Post> = getPosts()
        assertThat(JsonUtils.encodeToString(posts)).contains("2023-11-06T21:04:01.915653")
    }

    companion object {
        fun getPosts(): List<Post> {
            val response = FileUtils.readFile("/posts.json")
            return JsonUtils.decodeFromString(response)
        }
        fun getPostsDto(): List<PostDTO> {
            val response = FileUtils.readFile("/posts.json")
            return JsonUtils.decodeFromString<List<Post>>(response).map { it.toDto() }
        }

        const val POST_ID = "d0e9611d-7da7-4257-9a61-b28c28f60b60"
        const val MESSAGE = "mensaje 1"
        const val MESSAGE_2 = "mensaje 2"
    }
}
