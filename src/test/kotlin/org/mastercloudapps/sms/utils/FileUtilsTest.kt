package org.mastercloudapps.sms.utils

import java.io.File
import java.nio.file.Files
import java.nio.file.StandardOpenOption

class FileUtilsTest {

    companion object {

        fun getMockImage(): File {
            val byteArray = "image data".toByteArray()
            val tmpFile = Files.createTempFile("integrationTest", ".png").toFile()
            Files.write(tmpFile.toPath(), byteArray, StandardOpenOption.CREATE, StandardOpenOption.WRITE)
            return tmpFile
        }

        fun getMockImageByteArray(): ByteArray = "image data".toByteArray()

        const val IMAGE: String = "https://i.ibb.co/0fQmnDV/c883c2271440.gif"
        const val THUMB: String = "https://i.ibb.co/br7Q6WR/c883c2271440.gif"
    }
}
