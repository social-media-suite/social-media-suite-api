package org.mastercloudapps.sms.services

import kong.unirest.HttpMethod
import kong.unirest.MockClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mastercloudapps.sms.services.img.ImgBBService
import org.mastercloudapps.sms.util.FileUtils
import org.mastercloudapps.sms.utils.FileUtilsTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [ImgBBService::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ImgBBServiceTest(@Autowired val imgBBService: ImgBBService) {

    var mockClient: MockClient? = null

    @BeforeAll
    fun init() {
        mockClient = MockClient.register()
    }

    @Test
    fun `get image url`() {
        mockClient?.expect(HttpMethod.POST, "https://api.imgbb.com/1/upload?key=1234")?.thenReturn(getImageUrl())
        val (image, thumb) = imgBBService.getImageUrl(FileUtilsTest.getMockImage())
        assertThat(image).isEqualTo("https://i.ibb.co/0fQmnDV/c883c2271440.gif")
        assertThat(thumb).isEqualTo("https://i.ibb.co/br7Q6WR/c883c2271440.gif")
    }

    fun getImageUrl(): String = FileUtils.readFile("/imgbb.json")
}
