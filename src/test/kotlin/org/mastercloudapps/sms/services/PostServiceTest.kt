package org.mastercloudapps.sms.services

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.entity.Post
import org.mastercloudapps.sms.repository.PostRepository
import org.mastercloudapps.sms.services.img.ImgService
import org.mastercloudapps.sms.utils.FileUtilsTest
import org.mastercloudapps.sms.utils.FileUtilsTest.Companion.IMAGE
import org.mastercloudapps.sms.utils.FileUtilsTest.Companion.THUMB
import org.mastercloudapps.sms.utils.JsonUtilsTest
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.MESSAGE
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.MESSAGE_2
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.POST_ID
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Sort
import java.util.Optional

@SpringBootTest(classes = [PostService::class])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostServiceTest(@Autowired val postService: PostService) {

    @MockkBean
    private lateinit var postRepository: PostRepository

    @MockkBean
    private lateinit var imgService: ImgService

    @Test
    fun `get all posts`() {
        every { postRepository.findAll(Sort.by("updated").descending()) } returns JsonUtilsTest.getPosts().reversed()
        assertThat(postService.findAll()[0].text).isEqualTo(MESSAGE_2)
    }

    @Test
    fun `get a post by id`() {
        every { postRepository.findById(POST_ID) } returns Optional.of(JsonUtilsTest.getPosts()[0])
        assertThat(postService.getById(POST_ID).text).isEqualTo(MESSAGE)
    }

    @Test
    @Disabled
    fun `add a new post`() {
        val post: Post = JsonUtilsTest.getPosts()[0]
        every { imgService.getImageUrl(any()) } returns Pair(IMAGE, THUMB)
        every { postRepository.save(Post(any(), MESSAGE, IMAGE, THUMB, any(), any())) } returns post
        val postDto: PostDTO = postService.create(MESSAGE, FileUtilsTest.getMockImage())
        assertThat(postDto.text).isEqualTo(MESSAGE)
        assertThat(postDto.image).isEqualTo(IMAGE)
    }

    @Test
    fun `delete a post`() {
        every { postRepository.deleteById(POST_ID) } returns Unit
        postService.delete(POST_ID)
        verify(exactly = 1) { postRepository.deleteById(POST_ID) }
    }
}
