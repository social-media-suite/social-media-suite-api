package org.mastercloudapps.sms.extensions

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.entity.Post
import org.mastercloudapps.sms.utils.FileUtilsTest.Companion.IMAGE
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.MESSAGE_2
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.POST_ID
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.getPosts

class ExtensionsTest {

    @Test
    fun `map to Entity`() {
        val postDto = PostDTO(text = MESSAGE_2, image = IMAGE)
        assertThat(postDto.toEntity().id).isNotNull()
    }

    @Test
    fun `map to DTO`() {
        val posts: List<Post> = getPosts()
        assertThat(posts[0].toDto().id).isEqualTo(POST_ID)
    }
}
