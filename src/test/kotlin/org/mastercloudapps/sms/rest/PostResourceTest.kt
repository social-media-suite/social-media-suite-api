package org.mastercloudapps.sms.rest

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.exceptions.InternalErrorException
import org.mastercloudapps.sms.services.PostService
import org.mastercloudapps.sms.util.JsonUtils
import org.mastercloudapps.sms.utils.FileUtilsTest
import org.mastercloudapps.sms.utils.FileUtilsTest.Companion.IMAGE
import org.mastercloudapps.sms.utils.JsonUtilsTest
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.MESSAGE
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.MESSAGE_2
import org.mastercloudapps.sms.utils.JsonUtilsTest.Companion.POST_ID
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.multipart

@WebMvcTest
class PostResourceTest(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    private lateinit var postService: PostService

    private val imgFile = MockMultipartFile("image", "some-image.png", MediaType.IMAGE_PNG_VALUE, FileUtilsTest.getMockImageByteArray())

    @Test
    fun `get all posts`() {
        every { postService.findAll() } returns JsonUtilsTest.getPostsDto()

        mockMvc.get("/posts")
            .andExpect { status { isOk() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON) } }
            .andDo { println() }
            .andExpect {
                jsonPath("\$.[0].id") { value(POST_ID) }
                jsonPath("\$.[0].text") { value(MESSAGE) }
                jsonPath("\$.[0].image") { value(IMAGE) }
                jsonPath("\$.[0].added") { value("2023-11-06T20:09:48.993682") }
                jsonPath("\$.[1].updated") { value("2023-11-06T21:04:01.917653") }
            }
    }

    @Test
    fun `get posts by id`() {
        every { postService.getById(POST_ID) } returns JsonUtilsTest.getPostsDto()[0]

        mockMvc.get("/posts/$POST_ID")
            .andExpect { status { isOk() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON) } }
            .andDo { println() }
            .andExpect {
                jsonPath("\$.id") { value(POST_ID) }
                jsonPath("\$.text") { value(MESSAGE) }
                jsonPath("\$.image") { value(IMAGE) }
                jsonPath("\$.added") { value("2023-11-06T20:09:48.993682") }
                jsonPath("\$.updated") { value("2023-11-06T20:09:48.993682") }
            }
    }

    @Test
    fun `create a new post`() {
        val post = JsonUtilsTest.getPostsDto()[1]
        every { postService.create(MESSAGE_2, any()) } returns post

        val response: String = mockMvc.multipart("/posts") {
            param("text", MESSAGE_2)
            file(imgFile)
        }.andExpect { status { isCreated() } }
            .andDo { println() }
            .andReturn()
            .response.contentAsString

        val postResponse: PostDTO = JsonUtils.decodeFromString(response)
        assertThat(postResponse.text).isEqualTo(MESSAGE_2)
        assertThat(postResponse.image).isEqualTo("https://i.ibb.co/cy7NT2x/e4233179f631.png")
        assertThat(postResponse.thumb).isEqualTo("https://i.ibb.co/mR8GwGK/e4233179f631.png")
    }

    @Test
    fun `fail creating new post`() {
        every { postService.create(MESSAGE_2, any()) } throws InternalErrorException("some error getting image")

        mockMvc.multipart("/posts") {
            param("text", MESSAGE_2)
            file(imgFile)
        }
            .andExpect { status { isInternalServerError() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON) } }
            .andExpect {
                jsonPath("\$.message") { value("some error getting image") }
                jsonPath("\$.path") { value("/posts") }
            }
    }

    @Test
    fun `delete a post`() {
        every { postService.delete(POST_ID) } returns mockk()

        mockMvc.delete("/posts/$POST_ID").andExpect { status { isNoContent() } }
    }
}
