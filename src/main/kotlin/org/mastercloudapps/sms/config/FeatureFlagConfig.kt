package org.mastercloudapps.sms.config

import io.getunleash.DefaultUnleash
import io.getunleash.util.UnleashConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FeatureFlagConfig {

    @Value("\${ff.unleash.api}")
    val api: String = ""

    @Value("\${ff.unleash.instanceId}")
    val instanceId: String = ""

    @Value("\${ff.unleash.appName}")
    val app: String = ""

    private val log: Logger = LoggerFactory.getLogger(FeatureFlagConfig::class.java)

    @Bean
    fun unleash(): DefaultUnleash {
        return DefaultUnleash(
            UnleashConfig.builder()
                .appName(app)
                .instanceId(instanceId)
                .unleashAPI(api)
                .build()
        )
    }
}
