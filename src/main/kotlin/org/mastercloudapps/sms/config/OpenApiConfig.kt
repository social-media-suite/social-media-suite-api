package org.mastercloudapps.sms.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.servers.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {

    @Value("\${api.version}")
    val loadedVersion: String = ""

    @Value("\${api.url}")
    val apiUrl: String = ""

    @Bean
    fun customOpenAPI(): OpenAPI? {
        return OpenAPI().servers(
            listOf(
                Server().url(apiUrl)
            )
        )
            .info(
                Info().title("Post API")
                    .version(loadedVersion)
            )
    }
}
