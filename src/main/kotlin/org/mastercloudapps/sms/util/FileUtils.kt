package org.mastercloudapps.sms.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Base64
import java.util.UUID

class FileUtils {

    companion object Methods {

        private val log: Logger = LoggerFactory.getLogger(FileUtils::class.java)
        fun readFile(fileName: String): String = this::class.java.getResource(fileName)?.readText(Charsets.UTF_8) ?: throw Exception("error reading $fileName")
        fun getImgPath(tmpFolder: String): File {
            val folder: String = Paths.get(System.getProperty("user.dir"), tmpFolder).toString()
            return File("$folder/${UUID.randomUUID()}")
        }
        fun encodeToBase64(imageFile: File): String {
            val imageBytes = Files.readAllBytes(imageFile.toPath())
            return Base64.getEncoder().encodeToString(imageBytes)
        }

        fun removeFile(file: File) {
            if (!file.delete()) {
                log.warn("Error deleting file")
            }
        }
    }
}
