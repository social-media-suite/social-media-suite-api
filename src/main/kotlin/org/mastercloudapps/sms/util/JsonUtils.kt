package org.mastercloudapps.sms.util

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.encodeToString
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@OptIn(kotlinx.serialization.ExperimentalSerializationApi::class)
class JsonUtils<T> {

    companion object Methods {
        inline fun <reified T> decodeFromString(json: String, lenient: Boolean = false): T {
            if (lenient) {
                return Json { ignoreUnknownKeys = true; isLenient = true }.decodeFromString(json)
            }
            return Json { ignoreUnknownKeys = true }.decodeFromString(json)
        }
        inline fun <reified T> encodeToString(obj: T): String {
            return Json { encodeDefaults = true }.encodeToString(obj)
        }
    }

    @Serializer(forClass = LocalDateTime::class)
    object DateSerializer : KSerializer<LocalDateTime> {
        private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")

        override fun serialize(encoder: Encoder, value: LocalDateTime) {
            encoder.encodeString(value.format(formatter))
        }

        override fun deserialize(decoder: Decoder): LocalDateTime {
            return LocalDateTime.parse(decoder.decodeString(), formatter)
        }
    }
}
