package org.mastercloudapps.sms.extensions

import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.entity.Post
import java.time.LocalDateTime
import java.util.UUID

fun Post.toDto(): PostDTO {
    with(this) {
        return PostDTO(this.id, this.text, this.image, this.thumb, this.added, this.updated)
    }
}
fun PostDTO.toEntity(): Post {
    with(this) {
        return Post(this.id ?: UUID.randomUUID().toString(), this.text, this.image, this.thumb, this.added ?: LocalDateTime.now(), this.updated ?: LocalDateTime.now())
    }
}
