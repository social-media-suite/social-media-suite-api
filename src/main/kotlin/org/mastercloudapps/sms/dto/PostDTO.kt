package org.mastercloudapps.sms.dto

import kotlinx.serialization.Serializable
import org.mastercloudapps.sms.util.JsonUtils
import java.time.LocalDateTime

@Serializable
data class PostDTO(val id: String? = null, val text: String, var image: String, var thumb: String? = null, @Serializable(with = JsonUtils.DateSerializer::class) val added: LocalDateTime? = null, @Serializable(with = JsonUtils.DateSerializer::class) val updated: LocalDateTime? = null)
