package org.mastercloudapps.sms.services.img

import java.io.File

interface ImgService {
    fun getImageUrl(imgFile: File): Pair<String, String>
}
