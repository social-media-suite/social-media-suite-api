package org.mastercloudapps.sms.services.img

import com.jayway.jsonpath.JsonPath
import kong.unirest.HttpResponse
import kong.unirest.Unirest
import org.mastercloudapps.sms.exceptions.InternalErrorException
import org.mastercloudapps.sms.util.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File

/**
 * api.imgbb.com - Imgbb's API v1 allows to upload pictures.
 *
 * Parameters
 * key (required): The API key.
 * image (required):  A binary file, base64 data, or a URL for an image. (up to 32 MB)
 * name (optional): The name of the file, this is automatically detected if uploading a file with a POST and multipart / form-data
 * expiration (optional): Enable this if you want to force uploads to be auto deleted after certain time (in seconds 60-15552000)
 */
@Service
class ImgBBService() : ImgService {

    @Value("\${imgbb.api}")
    val url: String = ""

    @Value("\${imgbb.apiKey}")
    val apiKey: String = ""

    @Value("\${imgbb.expiration}")
    val expiration: Boolean = true

    private val log: Logger = LoggerFactory.getLogger(ImgBBService::class.java)

    // FF to call api imgBB or placeholder
    override fun getImageUrl(imgFile: File): Pair<String, String> {
        try {
            val imgBase64: String = FileUtils.encodeToBase64(imgFile)
            val jsonImgBB = uploadImg(imgBase64)
            val image = JsonPath.read<String>(jsonImgBB, "$.data.url")
            val thumb = JsonPath.read<String>(jsonImgBB, "$.data.thumb.url")
            return Pair(image, thumb)
        } catch (e: Exception) {
            throw InternalErrorException(e.message)
        }
    }

    private fun uploadImg(imgBase64: String): String {
        val expirationParam = if (expiration) { "&expiration=600" } else { "" }
        val imageRequest: HttpResponse<String> = Unirest.post("$url?key=$apiKey$expirationParam")
            .multiPartContent()
            .header("key", apiKey)
            .field("image", imgBase64)
            .asString()
        return imageRequest.body
    }
}
