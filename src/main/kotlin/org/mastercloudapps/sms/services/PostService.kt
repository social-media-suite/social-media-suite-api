package org.mastercloudapps.sms.services

import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.extensions.toDto
import org.mastercloudapps.sms.extensions.toEntity
import org.mastercloudapps.sms.repository.PostRepository
import org.mastercloudapps.sms.services.img.ImgService
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.io.File

@Service
class PostService(
    private val postRepository: PostRepository,
    private val imgService: ImgService
) {

    fun findAll(): List<PostDTO> =
        postRepository.findAll(Sort.by("updated").descending()).map { it.toDto() }.toList()

    fun getById(id: String): PostDTO =
        postRepository.findById(id).map { it.toDto() }.orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND, "This article does not exist") }

    fun create(text: String, imageFile: File): PostDTO {
        val (image, thumb) = imgService.getImageUrl(imageFile)
        val postDTO = PostDTO(text = text, image = image, thumb = thumb)
        return postRepository.save(postDTO.toEntity()).toDto()
    }

    fun delete(id: String) = postRepository.deleteById(id)
}
