package org.mastercloudapps.sms

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@OpenAPIDefinition
class App

fun main(args: Array<String>) {
    runApplication<App>(*args)
}
