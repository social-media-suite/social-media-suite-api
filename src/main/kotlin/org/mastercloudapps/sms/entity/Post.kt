package org.mastercloudapps.sms.entity

import kotlinx.serialization.Serializable
import org.mastercloudapps.sms.util.JsonUtils
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document(collection = "posts")
@Serializable
class Post(

    @Id
    var id: String? = null,
    val text: String,
    val image: String,
    val thumb: String?,
    @Serializable(with = JsonUtils.DateSerializer::class)
    val added: LocalDateTime = LocalDateTime.now(),
    @Serializable(with = JsonUtils.DateSerializer::class)
    val updated: LocalDateTime

)
