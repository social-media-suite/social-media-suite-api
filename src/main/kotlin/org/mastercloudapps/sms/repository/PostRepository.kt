package org.mastercloudapps.sms.repository

import org.mastercloudapps.sms.entity.Post
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface PostRepository : MongoRepository<Post, String>
