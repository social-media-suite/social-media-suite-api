package org.mastercloudapps.sms.exceptions

import jakarta.servlet.http.HttpServletRequest
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime

@ControllerAdvice
class CustomExceptionHandler : ResponseEntityExceptionHandler() {

    private val log: Logger = LoggerFactory.getLogger(CustomExceptionHandler::class.java)

    @ExceptionHandler(InternalErrorException::class, FileSizeLimitExceededException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun messageException(e: Exception, request: HttpServletRequest): ResponseEntity<Any> {
        log.error("Error 500: ${e.message}")
        val apiError = ApiErrorDTO(
            LocalDateTime.now(),
            HttpStatus.INTERNAL_SERVER_ERROR.value(),
            "Internal Server Error",
            request.requestURI,
            e.message
        )

        return ResponseEntity<Any>(apiError, HttpStatus.INTERNAL_SERVER_ERROR)
    }
}

data class ApiErrorDTO(
    val timestamp: LocalDateTime,
    val status: Int,
    val error: String,
    val path: String,
    val message: String?
)
