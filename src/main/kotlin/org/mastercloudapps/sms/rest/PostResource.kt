package org.mastercloudapps.sms.rest

import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.exceptions.InternalErrorException
import org.mastercloudapps.sms.services.PostService
import org.mastercloudapps.sms.util.FileUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.io.File

@RestController
@RequestMapping(
    value = ["/posts"],
    produces = [MediaType.APPLICATION_JSON_VALUE]
)
class PostResource(
    private val postService: PostService
) {

    private val log: Logger = LoggerFactory.getLogger(PostResource::class.java)

    @Value("\${tmp.folder}")
    val tmpFolder: String = ""

    @GetMapping
    fun getAllPosts(): ResponseEntity<List<PostDTO>> = ResponseEntity.ok(postService.findAll())

    @GetMapping("/{id}")
    fun getPosts(@PathVariable(name = "id") id: String): ResponseEntity<PostDTO> =
        ResponseEntity.ok(postService.getById(id))

    @PostMapping
    @ApiResponse(responseCode = "201")
    fun createPosts(
        @RequestParam("text")
        @RequestBody(description = "Caption for a picture")
        text: String,
        @RequestPart("image")
        @RequestBody(description = "Picture to be uploaded")
        image: MultipartFile
    ): ResponseEntity<PostDTO> {
        val imageFile: File = transferImage(image)
        try {
            val post = postService.create(text, imageFile)
            FileUtils.removeFile(imageFile)
            return ResponseEntity(post, HttpStatus.CREATED)
        } catch (e: Exception) {
            FileUtils.removeFile(imageFile)
            throw e
        }
    }

    @DeleteMapping("/{id}")
    @ApiResponse(responseCode = "204")
    fun deletePosts(@PathVariable(name = "id") id: String): ResponseEntity<Void> {
        postService.delete(id)
        return ResponseEntity.noContent().build()
    }

    fun transferImage(multiPartFile: MultipartFile): File {
        try {
            val targetFile = FileUtils.getImgPath(tmpFolder)
            multiPartFile.transferTo(targetFile)
            return targetFile
        } catch (e: Exception) {
            throw InternalErrorException(e.message)
        }
    }
}
