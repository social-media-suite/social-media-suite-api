package org.mastercloudapps.sms.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class IndexController {

    @Value("\${ui.url}")
    val uiUrl: String = ""

    @GetMapping("/")
    fun index(): ModelAndView = ModelAndView("redirect:$uiUrl")
}
