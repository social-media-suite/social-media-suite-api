package org.mastercloudapps.sms.rest

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import kong.unirest.HttpResponse
import kong.unirest.Unirest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mastercloudapps.sms.App
import org.mastercloudapps.sms.dto.PostDTO
import org.mastercloudapps.sms.repository.PostRepository
import org.mastercloudapps.sms.services.img.ImgService
import org.mastercloudapps.sms.util.JsonUtils
import org.mastercloudapps.sms.utils.FileUtilsTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.util.*

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [App::class]
)
@Testcontainers
class PostResourceMongoIntegrationTest(@Autowired val postRepository: PostRepository) : IntegrationTest() {

    @LocalServerPort
    var port: Int? = null

    @MockkBean
    private lateinit var imgService: ImgService

    @AfterEach
    fun cleanup() {
        postRepository.findAll(Sort.by("updated")).forEach { postRepository.deleteById(it.id!!) }
    }

    @Test
    fun `mongodb container is up and running`() {
        Assertions.assertTrue(mongoDBContainer.isRunning)
    }

    @Test
    fun `assert add a new a post and retrieve the data using mongodb`() {
        val request = getRequest()

        createPost(request.text, port)

        val posts = getPosts(port)
        org.assertj.core.api.Assertions.assertThat(posts.size).isEqualTo(1)

        val detail: HttpResponse<String> = Unirest.get("http://localhost:$port/posts/${posts[0].id}").asString()
        org.assertj.core.api.Assertions.assertThat(detail.isSuccess)

        val post: PostDTO = JsonUtils.decodeFromString(detail.body)
        org.assertj.core.api.Assertions.assertThat(post.id).isNotNull()
        org.assertj.core.api.Assertions.assertThat(post.text).contains(request.text)
        org.assertj.core.api.Assertions.assertThat(post.image).isEqualTo(FileUtilsTest.IMAGE)
        org.assertj.core.api.Assertions.assertThat(post.thumb).isEqualTo(FileUtilsTest.THUMB)
    }

    @Test
    fun `assert delete a post using mongodb`() {
        createPost(getRequest().text, port)

        val posts: List<PostDTO> = getPosts(port)
        org.assertj.core.api.Assertions.assertThat(posts.size).isEqualTo(1)

        val detail: HttpResponse<String> = Unirest.delete("http://localhost:$port/posts/${posts[0].id}").asString()
        org.assertj.core.api.Assertions.assertThat(detail.isSuccess)

        org.assertj.core.api.Assertions.assertThat(getPosts(port).size).isEqualTo(0)
    }

    @Test
    fun `post not found using mongodb`() {
        val id = UUID.randomUUID().toString()
        val response: HttpResponse<String> = Unirest.get("http://localhost:$port/posts/$id").asString()
        org.assertj.core.api.Assertions.assertThat(response.status).isEqualTo(HttpStatus.NOT_FOUND.value())
    }

    private fun getPosts(port: Int?): List<PostDTO> {
        val list: HttpResponse<String> = Unirest.get("http://localhost:$port/posts").asString()
        org.assertj.core.api.Assertions.assertThat(list.isSuccess)
        return JsonUtils.decodeFromString(list.body)
    }

    private fun createPost(text: String, port: Int?) {
        every { imgService.getImageUrl(any()) } returns Pair(FileUtilsTest.IMAGE, FileUtilsTest.THUMB)
        Unirest.post("http://localhost:$port/posts")
            .field("text", text)
            .field("image", FileUtilsTest.getMockImage(), "image/png")
            .asString()
    }
}

fun mongoDBContainer(imageName: String, opts: MongoDBContainer.() -> Unit) =
    MongoDBContainer(DockerImageName.parse(imageName)).apply(opts)
