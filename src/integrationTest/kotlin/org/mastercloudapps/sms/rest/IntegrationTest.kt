package org.mastercloudapps.sms.rest

import org.mastercloudapps.sms.dto.PostDTO
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.junit.jupiter.Container
import java.time.Duration

open class IntegrationTest {

    companion object {

        @Container
        val mongoDBContainer: MongoDBContainer = mongoDBContainer("mongo:6.0.6") {
            waitingFor(Wait.forLogMessage(".*Waiting for connections*\\s", 1))
            withStartupAttempts(3)
            withStartupTimeout(Duration.ofSeconds(120))
            start()
        }

        @JvmStatic
        @DynamicPropertySource
        fun datasourceConfig(registry: DynamicPropertyRegistry) {
            registry.add("spring.data.mongodb.port", mongoDBContainer::getFirstMappedPort)
            registry.add("spring.data.mongodb.host", mongoDBContainer::getHost)
            registry.add("spring.data.mongodb.database") { "posts" }
        }

        fun getRequest() = PostDTO(text = "some message", image = "https://loremflickr.com/640/360")
    }
}
