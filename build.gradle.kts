import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "3.1.5"
    id("io.spring.dependency-management") version "1.1.3"
    kotlin("jvm") version "1.8.22"
    kotlin("plugin.spring") version "1.8.22"
    kotlin("plugin.serialization") version "1.8.22"
    id("org.jetbrains.kotlin.plugin.noarg") version "1.7.10"
    id("org.sonarqube") version "4.0.0.2929"
    id("org.jlleitschuh.gradle.ktlint") version "11.6.1"
    jacoco
}

group = "org.mastercloudapps.sms"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

noArg {
    annotation("org.mastercloudapps.sms.entity.Post")
}

jacoco {
    toolVersion = "0.8.8"
}

springBoot {
    mainClass.set("org.mastercloudapps.sms.AppKt")
}

sonar {
    properties {
        property("sonar.projectKey", project.findProperty("sonar.projectName")!!)
        property("sonar.projectName", project.findProperty("sonar.projectName")!!)
        property("sonar.login", project.findProperty("sonar.login")!!)
        property("sonar.host.url", project.findProperty("sonar.host.url")!!)
        property("sonar.sources", "src/main")
        property("sonar.tests", "src/test")
        property("sonar.qualitygate.wait", true)
        property("sonar.coverage.exclusions", "**/config/**, **/App*")
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // OpenApi
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.2.0")

    // mongodb
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")

    // integration test
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    // mocks
    testImplementation("io.mockk:mockk:1.13.8")
    testImplementation("com.ninja-squad:springmockk:3.0.1")

    // test containers
    testImplementation("org.testcontainers:junit-jupiter")
    testImplementation("org.testcontainers:mongodb")

    // web client
    implementation("com.konghq:unirest-java:3.13.6")
    implementation("com.konghq:unirest-mocks:3.13.6")

    // json serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.0")
    implementation("com.jayway.jsonpath:json-path:2.6.0")

    // feature flag
    implementation("io.getunleash:unleash-client-java:6.0.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    jvmArgs("-Xshare:off")
}

/* not generating plain jar */
tasks.getByName<Jar>("jar") {
    enabled = false
}

tasks.test {
    testLogging {
        events("PASSED", "SKIPPED", "FAILED", "STANDARD_OUT", "STANDARD_ERROR")
    }
}

val integrationTest: SourceSet = sourceSets.create("integrationTest") {
    java {
        compileClasspath += sourceSets.main.get().output + sourceSets.test.get().output
        runtimeClasspath += sourceSets.main.get().output + sourceSets.test.get().output
        srcDir("src/integrationTest/java")
    }
    resources.srcDir("src/integrationTest/resources")
}

configurations[integrationTest.implementationConfigurationName].extendsFrom(configurations.testImplementation.get())
configurations[integrationTest.runtimeOnlyConfigurationName].extendsFrom(configurations.testRuntimeOnly.get())

tasks.check {
    dependsOn(integrationTestTask)
}

val integrationTestTask = tasks.register<Test>("integrationTest") {
    group = "verification"
    testClassesDirs = integrationTest.output.classesDirs
    classpath = sourceSets["integrationTest"].runtimeClasspath
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
    }
    classDirectories.setFrom(
        classDirectories.files.map {
            fileTree(it).matching {
                exclude(
                    "org/mastercloudapps/sms/App*",
                    "org/mastercloudapps/sms/config/**"
                )
            }
        }
    )
    dependsOn(tasks.test, integrationTestTask)
    finalizedBy(tasks.jacocoTestCoverageVerification)
}

tasks.jacocoTestCoverageVerification {
    executionData("build/jacoco/integrationTest.exec") // add integration test to test coverage verification
    violationRules {
        rule {
            limit {
                minimum = "0.8".toBigDecimal()
            }
        }
    }
}

tasks.ktlintCheck {
    dependsOn(tasks.ktlintFormat)
}
